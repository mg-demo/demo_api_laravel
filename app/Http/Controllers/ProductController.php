<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Product;

class ProductController extends Controller {
    public function index() {
        try {
            $products = Product::all();
            return response()->json([
                'result' => 'Ok',
                'message' => 'Productos obtenidos satisfactoriamente.',
                'products' => $products
            ], 201);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function show($id) {
        try {
            $product = Product::find($id);

            if (!$product) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'No se encontro productos con ID: ' . $id
                ], 409);
            }

            return response()->json([
                'result' => 'Ok',
                'message' => 'Producto obtenido satisfactoriamente.',
                'product' => $product
            ], 201);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function store(Request $request) {
        try {
            $product = Product::create([
                'code' => $request->code,
                'name' => $request->name,
                'description' => $request->description,
                'price' => $request->price,
                'image' => $request->image,
            ]);

            return response()->json([
                'result' => 'Ok',
                'message' => 'Producto creado satisfactoriamente.',
                'product' => $product
            ], 201);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function update(Request $request, $id) {
        try {
            $product = Product::find($id);

            if (!$product) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'No se encontro producto con ID: ' . $id,
                ], 409);
            }

            $product->update($request->all());
    
            return response()->json([
                'result' => 'Ok',
                'message' => 'Producto actualizado satisfactoriamente.',
                'product' => $product
            ], 200);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function updateImage(Request $request, $id) {
        try {
            $product = Product::find($id);

            if (!$product) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'No se encontro producto con ID: ' . $id,
                    // 'request' => $request->headers,
                ], 409);
            }

            try {
                if($request->hasFile('image')) {
                    $data['image'] = $request->image->store('');
                    $url = Storage::url($data['image']);

                    $product->update([
                        'image' => $url
                    ]);
                } else {
                    return response()->json([
                        'message' => 'Not loaded image',
                        'result' => 'Error'
                    ], 422);
                }
            } catch (Throwable $e) {
                return response()->json([
                    'message' => $e,
                    'result' => 'Error'
                ], 200);
            }

            return response()->json([
                'result' => 'Ok',
                'message' => 'Producto actualizado satisfactoriamente.',
                'product' => $product
            ], 200);

        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function delete($id) {
        try {
            $product = Product::find($id);

            if (!$product) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'No se encontro productos con ID: ' . $id,
                ], 409);
            }

            $affectedRows = $product->delete();

            return response()->json([
                'result' => 'Ok',
                'message' => 'Producto eliminado satisfactoriamente.',
                'affectedRows' => $affectedRows,
            ], 204);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }
}
