<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserController extends Controller {
    public function index() {
        try {
            $users = User::all();
            return response()->json([
                'result' => 'Ok',
                'message' => 'Usuarios obtenidos satisfactoriamente.',
                'users' => $users
            ], 201);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function show($id) {
        try {
            $user = User::find($id);

            if (!$user) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'No se encontro usuario con ID: ' . $id
                ], 409);
            }

            return response()->json([
                'result' => 'Ok',
                'message' => 'Usuario obtenido satisfactoriamente.',
                'user' => $user
            ], 201);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function store(Request $request) {
        try {
            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password)
            ]);

            return response()->json([
                'result' => 'Ok',
                'message' => 'Usuario creado satisfactoriamente.',
                'user' => $user
            ], 201);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function update(Request $request, $id) {
        try {
            $user = User::find($id);

            if (!$user) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'No se encontro usuario con ID: ' . $id,
                ], 409);
            }

            $user->update($request->all());
    
            return response()->json([
                'result' => 'Ok',
                'message' => 'Usuario actualizado satisfactoriamente.',
                'user' => $user
            ], 200);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }

    public function delete($id) {
        try {
            $user = User::find($id);

            if (!$user) {
                return response()->json([
                    'result' => 'Error',
                    'message' => 'No se encontro usuario con ID: ' . $id,
                ], 409);
            }

            $affectedRows = $user->delete();

            return response()->json([
                'result' => 'Ok',
                'message' => 'Usuario eliminado satisfactoriamente.',
                'affectedRows' => $affectedRows,
            ], 204);
        } catch (Exception $exception) {
            return response()->json([
                'result' => 'Error',
                'message' => $exception->errors()
            ], 409);
        }
    }
}
