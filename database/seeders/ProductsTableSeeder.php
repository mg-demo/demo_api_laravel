<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Let's clear the products table first
        Product::truncate();

        Product::create([
            'code' => 'ABR-001',
            'name' => 'Aceite',
            'description' => 'Descripción del aceite',
            'price' => 100.0,
        ]);

        Product::create([
            'code' => 'ABR-002',
            'name' => 'Arroz',
            'description' => 'Descripción del arroz',
            'price' => 100.0,
        ]);

        Product::create([
            'code' => 'ABR-003',
            'name' => 'Azucar',
            'description' => 'Descripción del azucar',
            'price' => 100.0,
        ]);

        Product::create([
            'code' => 'ABR-004',
            'name' => 'Café',
            'description' => 'Descripción del café',
            'price' => 100.0,
        ]);

        Product::create([
            'code' => 'ABR-005',
            'name' => 'Fideos',
            'description' => 'Descripción del fideos',
            'price' => 100.0,
        ]);

        Product::create([
            'code' => 'ABR-006',
            'name' => 'Fosforos',
            'description' => 'Descripción del fosforos',
            'price' => 100.0,
        ]);

        Product::create([
            'code' => 'ABR-007',
            'name' => 'Harina',
            'description' => 'Descripción del harina',
            'price' => 100.0,
        ]);

        Product::create([
            'code' => 'ABR-008',
            'name' => 'Té',
            'description' => 'Descripción del té',
            'price' => 100.0,
        ]);

    }
}
